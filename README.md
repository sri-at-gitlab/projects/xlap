![Xlap Icon - Smiling Monkey Face](./icon.png)

# Xlap

Window snap assistant for Xfce and the X Window System

![Xlap Preview](./preview-800.gif)

#### Features

- [x] Hotkeys / keyboard shortcuts
- [x] Indicator menu
- [x] Multi-display support
- [x] Gaps between windows
- [x] Configurable

## Install

- Arch or Arch based distros:
    - `yay xlap`
    - AUR: https://aur.archlinux.org/packages/xlap/
- TODO - Distribution packages for many distros are not yet available
- For now:
    - Ensure [system deps](#system-dependencies) are locally installed
    - See the [local execution](#local-execution) section

### System Dependencies

- X Display Server
- `xdotool`
- `xprop`
- `xdg-open`
- Python 3
- `python-gobject`
- `libappindicator-gtk3` (includes `AppIndicator3` or `AyatanaAppIndicator3`)
- `python-pynput`
- `notify-send`

### Local Execution

1. Clone this repo or [download archive](https://gitlab.com/sri-at-gitlab/projects/xlap/-/archive/main/xlap-main.zip) and extract
2. Navigate to folder
3. Execute `./xlap`

## Usage

#### Indicator Menu

- Select the `monkey-face` icon in your indicator to view the layout options
- Select your preferred layout option and it will be applied to the active window

#### Keyboard Shortcuts

- `Super + Alt + Right Arrow` to set active window to the next layout option
- `Super + Alt + Left Arrow` to set active window to the previous layout option

`Super` is interchangeable with `Cmd` on some keyboards.

#### Configuration

- Configuration is read from `~/.xlap-conf.json`
    - On first run, this file is created
- Properties
    - `window_margin_top: int` margin top for each window
    - `window_margin_left: int` margin left for each window
    - `screen_margin_bottom: int` margin bottom for each screen
    - `screen_margin_right: int` margin right for each screen
    - `notify_on_apply_layout: bool` confirmation notifications on window layout apply
    - `notify_on_launch: bool` notification on app launch

## Contribute

- Send a Merge Request to this project on GitLab
- Reach out to me via:
    - [Thread on XFCE Forum](https://forum.xfce.org/viewtopic.php?id=15170)
    - [Thread on Reddit](https://www.reddit.com/r/xfce/comments/p6p1cv/i_want_to_create_a_advanced_window_snapping_app/)
    - [opensource@srirangan.net](mailto:opensource@srirangan.net)

## License

[GNU GPLv3](./LICENSE)

## Credits & Inspiration

- [xdotool](https://github.com/jordansissel/xdotool)
- [wmctrl](https://linux.die.net/man/1/wmctrl)
- [Rectangle](https://github.com/rxhanson/Rectangle)
- XFCE Forum:
    - [ToZ](https://forum.xfce.org/profile.php?id=18944)
    - [Misko_2083](https://forum.xfce.org/profile.php?id=22574)
- Reddit r/xfce:
    - Illiamen
    - krncnr
- [Ryan Hanson](https://github.com/rxhanson) for [donating Rectangle icons](https://github.com/rxhanson/Rectangle/discussions/550)
